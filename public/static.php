<?php

require "../vendor/autoload.php";
use App\User;
use App\StringLengthException;
use App\EmailFormatException;


$test = [
  [
    "name" => "Joël",
    "surname" => "Robuchon",
    "birthdate" => "1945-04-07",
    "email" => "jojo@gmail.com",
    "password" => "choucroutte4ever"
  ],
  [
    "name" => "Maïté",
    "surname" => "Ordonez",
    "birthdate" => "1938-06-02",
    "email" => "marie-therese@gmail.com",
    "password" => "PeuchèreChocolatine"
  ],
  [
    "name" => "Gordon",
    "surname" => "Ramsay",
    "birthdate" => "1966-11-08",
    "email" => "flash-gordon@gmail.com",
    "password" => "WhatIsThatShiz?"
  ]
];
try {
  $users = User::getAll($test);

  echo "<pre>";
  echo $users[1];
  echo "</pre>";

} catch (StringLengthException $error) {
  echo "erreur le longeur de chaîne : {$error->getMessage()}";
} catch (EmailFormatException $error) {
  echo "erreur de format d'email : {$error->getMessage()}";
} 

