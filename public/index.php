<?php
require "../vendor/autoload.php";

use App\User;
use App\StringLengthException;
use App\EmailValidationException;

// $maVariable = "tfhyo";

// try {
//   if (strlen($maVariable) < 3) {
//     throw new Exception ("erreur");
//   }
//   // echo "no error";
// } catch (Exception $error) {
//   echo $error->getMessage();
// }

$userInput = filter_input_array(INPUT_POST,FILTER_SANITIZE_SPECIAL_CHARS);

$date = new DateTime($userInput["birthdate"]);
try {
  $user = new User($userInput["name"], $userInput["surname"], $date, $userInput["email"],$userInput["password"]);
} catch (StringLengthException $error) {
  echo "Erreur de nombre de caractères : {$error->getMessage()}";
} catch (EmailFormatException $error) {
  echo "Erreur de format d'email : {$error->getMessage()}";
}
